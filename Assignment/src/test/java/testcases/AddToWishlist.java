package testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pageObjects.HomePage;
import utilities.JsonReader;

public class AddToWishlist extends TestBase{
	HomePage homePage;
	String userName;
	String password;
	JsonReader jsonReader;
	
	@BeforeClass
	public void init() {
		startDriver();
		homePage = new HomePage(driver);
	}
	
	@Test
	public void add2Items() {
		jsonReader = new JsonReader();
		String[] logins = jsonReader.getLogins();
		userName = logins[1];
		password = logins[0];
		homePage.clickOnLogin().login(userName, password).returnToHome().clickOnClothing().clickOnFirstItem().addToWishList().navigateToWishList().validateWishList();
	}
	
	@AfterTest
	public void stopDriver() {
		driver.quit();
	}
}
