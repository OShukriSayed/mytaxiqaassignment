package testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pageObjects.HomePage;

public class Add2ItemsTest extends TestBase {
	HomePage homePage;

	@BeforeClass
	public void init() {
		startDriver();
		homePage = new HomePage(driver);
	}

	@Test
	public void add2Items() {
		homePage.clickOnClothing().clickOnFirstItem().selectAnItemAddTwice().clickOnCartButton().validateCart();
	}

	@AfterTest
	public void stopDriver() {
		driver.quit();
	}
}
