package testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestBase {
	public static WebDriver driver;
	public final String baseUrl = "https://www.zalando.co.uk/";

	public void startDriver() {
		if ((driver) == null) {
			ChromeOptions cOptions = new ChromeOptions();
			cOptions.addArguments("--start-maximized");
			cOptions.addArguments("--incognito");
			cOptions.addArguments("--disable-popup-blocking");
			driver = new ChromeDriver(cOptions);
		}
		driver.navigate().to(baseUrl);
	}

}
