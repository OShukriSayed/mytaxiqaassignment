package utilities;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonReader {

	JSONParser parser;
	JSONObject jsonObj;

	public JsonReader() {
		try {
			parser = new JSONParser();
			jsonObj = (JSONObject) parser
					.parse(new FileReader(System.getProperty("user.dir") + "/resources/Data.json"));
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}

	public String[] getLogins() {
		String[] logins = new String[2];
		JSONObject jsonObjElement = (JSONObject) jsonObj.get("login");
		logins[0] = jsonObjElement.get("password").toString();
		logins[1] = jsonObjElement.get("username").toString();
		return logins;
	}
}
