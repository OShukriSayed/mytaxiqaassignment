package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CartPage extends PageBase {

	@FindBy(xpath = "//select[@class='z-coast-fjord_quantitySelect']/option[@selected]")
	private WebElement txtQuantityValue;

	@FindBy(xpath = "//div[@class='z-coast-fjord_totalBox']/z-grid[2]/z-grid-item[2]/div/div/span")
	private WebElement txtCartTotal;

	@FindBy(xpath = "//div[@class='z-coast-fjord_totalBox']/z-grid[1]/z-grid-item[2]/div/div/span")
	private WebElement txtDelivery;

	@FindBy(xpath = "//div[@class='z-coast-fjord_articleGroup']/div[1]/z-grid[1]/z-grid-item[2]/div/span/a")
	private WebElement txtFirstItemName;

	private int itemQuantity;

	private double itemPrice;

	private String itemName;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public CartPage(WebDriver driver) {
		super(driver);
	}

	public CartPage validateCart() {
		int quantity = Integer.parseInt(txtQuantityValue.getAttribute("value"));
		double total = Double.parseDouble(txtCartTotal.getText().substring(1));
		String itemName = txtFirstItemName.getText();
		Assert.assertEquals(itemName, getItemName());
		Assert.assertEquals(quantity, getItemQuantity());
		Assert.assertEquals(total, getItemQuantity() * getItemPrice() + getDelivery());
		cleanUp();
		return this;
	}

	private void cleanUp() {
		Boolean isPresent = driver
				.findElements(By.xpath("//span[@class='z-coast-fjord_removeArticle z-coast-fjord_interactable']"))
				.size() > 0;
		while (isPresent) {
			driver.findElement(By.xpath("//span[@class='z-coast-fjord_removeArticle z-coast-fjord_interactable']"))
					.click();
			isPresent = driver
					.findElements(By.xpath("//span[@class='z-coast-fjord_removeArticle z-coast-fjord_interactable']"))
					.size() > 0;
		}
	}

	private double getDelivery() {
		String deliveryValueText = txtDelivery.getText();
		double deliveryPrice;
		if (deliveryValueText.equals("free")) {
			deliveryPrice = 0;
		} else {
			deliveryPrice = Double.parseDouble(deliveryValueText.substring(1));
		}
		return deliveryPrice;
	}
}
