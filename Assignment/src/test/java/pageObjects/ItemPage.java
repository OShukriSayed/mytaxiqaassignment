package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class ItemPage extends PageBase {
	
	@FindBy (css = "span[class='z-text zvui-product-price-display zvui-product-price-actual large z-text-title-3 z-text-black']")
	private WebElement txtItemPrice;

	@FindBy(xpath = "//div[@class='z-vegas-pdp_sizeSelection']/div/div")
	private WebElement ddlSizeDropDownList;

	@FindBy(css = "button[class='z-rich-button z-rich-button--primary']")
	private WebElement btnAddToBag;

	@FindBy(xpath = "//a[@href='/cart/']")
	private WebElement btnCart;
	
	@FindBy(css = "span[class='z-text zvui-product-title-brandname z-text-block z-text-body z-text-black']")
	private WebElement txtItemName;

	@FindBy(css = "button[class='z-button z-vegas-ui_iconButton z-button--tertiary z-button--button z-button--no-animation z-button--light']")
	private WebElement btnAddToWishList;
	
	@FindBy(css = "a[href='/wishlist/']")
	private WebElement lnkWishListPage;
	
	public ItemPage(WebDriver driver) {
		super(driver);
	}
	
	public double getTxtItemPrice() {
		String price =txtItemPrice.getText().substring(1);
		return Double.parseDouble(price);
	}

	private void clickOnSizeDdl() {
		ddlSizeDropDownList.click();
	}

	private void chooseFirstSize() {
		waitForElementToBeVisible(By.xpath("//div[@class='zvui-size-select-dropdown zvui-size-select-dropdown-single focused is-open']"));
		//size changed to third option, as first is out of stock and causes a popup
		WebElement firstChose = driver
				.findElement(By.xpath("//div[@class='zvui-size-select-dropdown-menu']/div[3]"));
		firstChose.click();
	}

	private void addItem() {
		btnAddToBag.click();
	}

	private void waitForButtonChange() {
		waitForElementToBeVisible(By.cssSelector(
				"button[class='z-rich-button z-rich-button--primary z-rich-button--state-change z-rich-button--success-state']"));
		waitForElementToBeVisible(By.cssSelector("button[class='z-rich-button z-rich-button--primary']"));
		Actions builder = new Actions(driver);
		builder.moveToElement(txtItemPrice).perform();
		waitForElementToBeNotVisible(By.cssSelector("button[class='z-button z-button--primary z-button--button']"));
	}

	public ItemPage selectAnItemAddTwice() {
		clickOnSizeDdl();
		chooseFirstSize();
		addItem();
		waitForButtonChange();
		addItem();
		waitForButtonChange();
		return this;
	}

	public CartPage clickOnCartButton() {
		double itemPrice=getTxtItemPrice();
		String itemName= txtItemName.getText();
		btnCart.click();
		CartPage cartPage = new CartPage (driver);
		cartPage.setItemPrice(itemPrice);
		cartPage.setItemQuantity(2);
		cartPage.setItemName(itemName);
		return cartPage;
	}

	public ItemPage addToWishList() {
		btnAddToWishList.click();
		return this;
	}

	public WishListPage navigateToWishList() {
		String itemName = txtItemName.getText();
		lnkWishListPage.click();
		WishListPage wishListPage = new WishListPage(driver);
		wishListPage.setItemName(itemName);
		return wishListPage;
	}
}
