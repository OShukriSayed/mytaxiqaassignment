package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageBase{
	
	@FindBy(xpath = "//input[@name='login.email']")
	private WebElement txtEmailAddress;

	@FindBy(xpath = "//input[@name='login.password']")
	private WebElement txtPassword;
	
	@FindBy(xpath = "//button[@class='z-button z-coast-reef_login_button z-button--primary z-button--button']")
	private WebElement btnLogin;

	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	public MyAccountPage login(String email, String password) {
		txtEmailAddress.clear();
		txtEmailAddress.sendKeys(email);
		txtPassword.clear();
		txtPassword.sendKeys(password);
		btnLogin.click();
		MyAccountPage myAccountPage = new MyAccountPage(driver);
		return myAccountPage;
	}

}
