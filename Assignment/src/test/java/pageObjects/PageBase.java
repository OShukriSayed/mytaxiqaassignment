package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageBase {
	protected WebDriver driver;

	public PageBase(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void waitForElementToBeVisible(By selector) {
		WebDriverWait wait = new WebDriverWait(driver, 240);
		wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
	}
	
	public void waitForElementToBeNotVisible(By selector) {
		WebDriverWait wait = new WebDriverWait(driver, 240);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(selector));
	}
}
