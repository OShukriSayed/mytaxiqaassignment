package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageBase {

	@FindBy(xpath = "//a[@href='/clothing/']")
	private WebElement lnkClothing;

	@FindBy(xpath = "//a[@href='/myaccount/']")
	private WebElement lnkMyAccount;

	public HomePage(WebDriver driver) {
		super(driver);
	}

	public ClothingPage clickOnClothing() {
		Boolean isPresent = driver.findElements(By.xpath("//a[@class='closeButton geoIpWarning_closeButton iconFont']"))
				.size() > 0;
		if (isPresent) {
			driver.findElement(By.xpath("//a[@class='closeButton geoIpWarning_closeButton iconFont']")).click();
		}
		lnkClothing.click();
		ClothingPage clothingPage = new ClothingPage(driver);
		return clothingPage;
	}

	public LoginPage clickOnLogin() {
		lnkMyAccount.click();
		LoginPage loginPage = new LoginPage(driver);
		return loginPage;
	}

}
