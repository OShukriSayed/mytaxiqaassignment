package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyAccountPage extends PageBase{
	
	@FindBy(xpath = "//div[@id='header-logo-container']")
	private WebElement lnkHeaderLogo;

	public MyAccountPage(WebDriver driver) {
		super(driver);
	}

	public HomePage returnToHome() {
		waitForElementToBeVisible(By.xpath ("//div[@id='header-logo-container']"));
		lnkHeaderLogo.click();
		HomePage homePage = new HomePage(driver);
		return homePage;
	}

}
