package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class WishListPage extends PageBase {

	@FindBy(xpath = "//div[@id='z-aladdin-cardList']/div/div[1]/div[3]/div/div/div/div[1]")
	private WebElement lblItemName;
	
	@FindBy(xpath = "//a[@href='/myaccount/']")
	private WebElement btnMyAccount;

	private String itemName;

	public WishListPage(WebDriver driver) {
		super(driver);
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemName() {
		return itemName;
	}

	public WishListPage validateWishList() {
		Assert.assertEquals(lblItemName.getText(), getItemName());
		cleanUp();
		return this;
	}

	private void cleanUp() {
		Boolean isPresent = driver
				.findElements(By
						.xpath("//div[@class='z_wishlist_ArticleCard__icon_close___ynls0 z_wishlist_icon__icon_remove___1YAnw z_wishlist_icon__icon___1PeMX z_wishlist_icon__icon-close___M326s']"))
				.size() > 0;
		while (isPresent) {
			driver.findElement(By
					.xpath("//div[@class='z_wishlist_ArticleCard__icon_close___ynls0 z_wishlist_icon__icon_remove___1YAnw z_wishlist_icon__icon___1PeMX z_wishlist_icon__icon-close___M326s']"))
					.click();
			isPresent = driver
					.findElements(By
							.xpath("//div[@class='z_wishlist_ArticleCard__icon_close___ynls0 z_wishlist_icon__icon_remove___1YAnw z_wishlist_icon__icon___1PeMX z_wishlist_icon__icon-close___M326s']"))
					.size() > 0;
		}
		
	signOut();
	}

	private void signOut() {
		btnMyAccount.click();
		Actions builder = new Actions(driver);
		builder.moveToElement(btnMyAccount).perform();
		waitForElementToBeVisible(By.xpath("//a[@name='head.text:logout.x:1.y:6']"));
		driver.findElement(By.xpath("//a[@name='head.text:logout.x:1.y:6']")).click();
	}

}
