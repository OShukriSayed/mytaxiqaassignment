package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ClothingPage extends PageBase {

	@FindBy(xpath = "//z-grid[@class='z-nvg-cognac_articles']/z-grid-item[1]/div/a")
	private WebElement lnkFirstClothingItem;
	
	public ClothingPage(WebDriver driver) {
		super(driver);
	}
	
	public ItemPage clickOnFirstItem(){
		lnkFirstClothingItem.click();
		ItemPage itemPage = new ItemPage(driver);
		return itemPage;
	}

}
